<?php

namespace App\Console\Commands\Download;

use App\Models\MediaItem;
use App\Models\Series as SeriesModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

use function Laravel\Prompts\multiselect;
use function Laravel\Prompts\progress;
use function Laravel\Prompts\spin;
use function Laravel\Prompts\text;

class Series extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:download-series-media';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download all media for one or more series';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $series = multiselect(
            label: 'Choose one or more series to download media',
            options: SeriesModel::pluck('title', 'id'),
            required: true,
            scroll: 10,
        );

        $authorization = text(
            label: 'Enter the Bearer authorization header',
            placeholder: 'Bearer eyJ…',
            required: true,
        );

        $destination = text(
            label: 'Where should files be saved?',
            default: storage_path(),
        );

        $destination = str($destination)->rtrim('/');

        $this->info('Starting download');

        progress(
            label: 'Downloading media',
            steps: SeriesModel::find($series)->load('media_items.series')->pluck('media_items')->flatten(1),
            callback: function (MediaItem $item) use ($authorization, $destination) {
                spin(
                    callback: function() use ($item, $authorization, $destination) {
                        $results = $item->download($authorization, $destination.'/'.$item->series->title);

                        if (! $results) {
                            $message = __('Downloading :item from :series failed', [
                                'item' => $item->title,
                                'series' => $item->series->title,
                            ]);
                            Log::error($message);
                            $this->error($message);
                        }
                    },
                    message: __('Downloading :item from :series', [
                        'item' => $item->title,
                        'series' => $item->series->title,
                    ]),
                );

                Log::info(__('Finished downloading :item from :series', [
                    'item' => $item->title,
                    'series' => $item->series->title,
                ]));
            },
        );

        return self::SUCCESS;
    }
}
