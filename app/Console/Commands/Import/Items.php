<?php

namespace App\Console\Commands\Import;

use App\Models\MediaItem;
use App\Models\Series;
use Illuminate\Console\Command;
use Illuminate\Contracts\Console\PromptsForMissingInput;
use Illuminate\Support\Facades\Http;

use function Laravel\Prompts\search;
use function Laravel\Prompts\text;

class Items extends Command implements PromptsForMissingInput
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-media {series} {authorization}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all items for the given series';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $seriesId = $this->argument('series');
        $series = Series::where('subsplash_id', $seriesId)->sole();

        $authorization = str($this->argument('authorization'))->after('Bearer ');

        $this->info('Starting import…');

        $url = urlencode('https://core.subsplash.com/media/v1/media-items?filter%5Bbroadcast.status%7Cbroadcast.status%5D=null%7Con-demand&filter%5Bmedia_series%5D='.$seriesId.'&filter%5Bstatus%5D=published&include=images%2Caudio.audio-outputs%2Caudio.video%2Cvideo.video-outputs%2Cdocument&page%5Bnumber%5D=1&page%5Bsize%5D=15&sort=-position');
        do {
            $results = Http::asJson()
                ->withToken($authorization)
                ->get(urldecode($url));

            $results->collect('_embedded.media-items')->map(function (array $row) use ($series) {
                $mediaItem = MediaItem::firstOrNew([
                    'subsplash_id' => data_get($row, 'id'),
                    'subsplash_short_code' => data_get($row, 'short_code'),
                ]);
                $mediaItem->forceFill([
                    'series_id' => $series->id,
                    'title' => data_get($row, 'title'),
                    'date' => data_get($row, 'date'),
                    'speaker' => data_get($row, 'speaker'),
                    'summary' => data_get($row, 'summary'),
                    'status' => data_get($row, 'status'),
                    'images' => collect(data_get($row, '_embedded.images'))->pluck('_links.self.href'),
                    'mp3_url' => data_get($row, '_embedded.audio._embedded.audio-outputs.0._links.self.href'),
                    'mp3_duration' => data_get($row, '_embedded.audio._embedded.audio-outputs.0.duration'),
                    'mp3_filesize' => data_get($row, '_embedded.audio._embedded.audio-outputs.0.file_size'),
                    'video_url' => data_get($row, '_embedded.audio._embedded.video._links.self.href'),
                    'video_duration' => data_get($row, '_embedded.audio._embedded.video.duration'),
                    'video_filesize' => data_get($row, '_embedded.audio._embedded.video.file_size'),
                    'other_urls' => collect([
                        data_get($row, 'web_video_button_title') => data_get($row, 'web_video_button_url'),
                        data_get($row, 'website_button_title') => data_get($row, 'website_button_url'),
                    ])->filter(),
                    'created_at' => data_get($row, 'created_at'),
                    'updated_at' => data_get($row, 'updated_at'),
                ]);
                $mediaItem->save();
            });

            $url = $results->json('_links.next.href');
        } while (filled($url) && $url !== $results->json('_links.last.href'));

        $this->info(__('The database has :count items.', [
            'count' => MediaItem::count(),
        ]));

        return self::SUCCESS;
    }

    protected function promptForMissingArgumentsUsing(): array
    {
        return [
            'series' => search(
                label: 'Choose a series',
                options: fn () => Series::pluck('title', 'subsplash_id')->all(),
            ),
            'authorization' => text(
                label: 'Enter the Bearer authorization header',
                placeholder: 'Bearer eyJ…',
                required: true,
            ),
        ];
    }
}
