<?php

namespace App\Console\Commands\Import;

use App\Models\Series as SeriesModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

use function Laravel\Prompts\text;

class Series extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-series';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all series';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $url = text(
            label: 'Enter the Subsplash URL',
            placeholder: 'https://core.subsplash.com/builder/v1/list-rows…',
            required: true,
        );

        $authorization = text(
            label: 'Enter the Bearer authorization header',
            placeholder: 'Bearer eyJ…',
            required: true,
        );

        $this->info('Starting import…');

        do {
            $results = Http::asJson()
                ->withToken(str($authorization)->after('Bearer '))
                ->get(urldecode($url));

            $results->collect('_embedded.list-rows')->map(function (array $row) {
                $series = SeriesModel::firstOrNew([
                    'subsplash_id' => data_get($row, '_embedded.media-series.id'),
                    'subsplash_short_code' => data_get($row, '_embedded.media-series.short_code'),
                ]);
                $series->forceFill([
                    'title' => data_get($row, '_embedded.media-series.title'),
                    'subtitle' => data_get($row, '_embedded.media-series.subtitle'),
                    'images' => collect(data_get($row, '_embedded.media-series._embedded.images'))->pluck('_links.self.href'),
                    'status' => data_get($row, '_embedded.media-series.status'),
                    'created_at' => data_get($row, '_embedded.media-series.created_at'),
                    'updated_at' => data_get($row, '_embedded.media-series.updated_at'),
                ]);
                $series->save();
            });

            $url = $results->json('_links.next.href');
        } while ($url !== $results->json('_links.last.href'));

        $this->info(__('The database has :count series.', [
            'count' => SeriesModel::count(),
        ]));

        if ($this->confirm('Would you like to import all the media items now?')) {
            SeriesModel::all()->each(fn (SeriesModel $series) => $this->call('app:import-media', [
                'series' => $series->subsplash_id,
                'authorization' => $authorization,
            ]));
        }

        return self::SUCCESS;
    }
}
