<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;

/**
 * @property int $id
 * @property int $series_id
 * @property string $subsplash_id
 * @property string $subsplash_short_code
 * @property string $title
 * @property \Illuminate\Support\Carbon|null $date
 * @property string|null $speaker
 * @property string|null $summary
 * @property string $status
 * @property array|null $images
 * @property string|null $mp3_url
 * @property string|null $mp3_duration
 * @property int|null $mp3_filesize
 * @property string|null $video_url
 * @property string|null $video_duration
 * @property int|null $video_filesize
 * @property array|null $other_urls
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Series $series
 *
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereMp3Duration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereMp3Filesize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereMp3Url($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereOtherUrls($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereSeriesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereSpeaker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereSubsplashId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereSubsplashShortCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereSummary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereVideoDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereVideoFilesize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MediaItem whereVideoUrl($value)
 *
 * @mixin \Eloquent
 */
class MediaItem extends Model
{
    protected $casts = [
        'images' => 'array',
        'other_urls' => 'array',
        'date' => 'date',
    ];

    protected $fillable = [
        'subsplash_id',
        'subsplash_short_code',
    ];

    public function series(): BelongsTo
    {
        return $this->belongsTo(Series::class);
    }

    public function download(string $authorization, ?string $directory = null): bool
    {
        if (! $this->mp3_url) {
            return false;
        }

        $authorization = str($authorization)->after('Bearer ');

        $path = Http::withToken($authorization)
            ->get($this->mp3_url)
            ->json('_links.download.href');

        $filename = str($path)->replace('{filename}', urlencode($this->title));

        if (! $filename) {
            return false;
        }

        $directory = filled($directory)
            ? str($directory)->rtrim('/')
            : storage_path();

        File::ensureDirectoryExists($directory);

        $destination = sprintf(
            '%s/%s - %s.mp3',
            $directory,
            $this->date?->format('Y-m-d') ?? 'Unknown Date',
            $this->title,
        );

        if (File::exists($destination)) {
            return true;
        }

        return Http::asJson()
            ->sink($destination)
            ->get($filename)
            ->ok();
    }
}
