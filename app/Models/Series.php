<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int $id
 * @property string $subsplash_id
 * @property string $subsplash_short_code
 * @property string $title
 * @property string|null $subtitle
 * @property array|null $images
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\MediaItem> $media_items
 * @property-read int|null $media_items_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Series newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Series newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Series query()
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereSubsplashId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereSubsplashShortCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Series whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Series extends Model
{
    protected $casts = [
        'images' => 'array',
    ];

    protected $fillable = [
        'subsplash_id',
        'subsplash_short_code',
    ];

    public function media_items(): HasMany
    {
        return $this->hasMany(MediaItem::class);
    }
}
