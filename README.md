# Subsplash Exporter

Exports a list of media from a Subsplash account.

## Usage

1. Open your browser’s dev tools
1. Point your browser to a page using the Subpslash embed feature
1. Find the network request `list-rows`
1. Run `php artisan app:import-series`
   - Provide the URL from the network request
   - Provide the authorization header from the network request

If you choose not to import all media items, you may later import media items for each series by running the command `php artisan app:import-media`
