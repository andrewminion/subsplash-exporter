<?php

use App\Models\Series;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('media_items', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Series::class)->constrained();
            $table->string('subsplash_id')->unique();
            $table->string('subsplash_short_code')->unique();
            $table->text('title');
            $table->timestamp('date')->nullable();
            $table->string('speaker')->nullable();
            $table->text('summary')->nullable();
            $table->string('status');
            $table->json('images')->nullable();
            $table->string('mp3_url')->nullable();
            $table->string('mp3_duration')->nullable();
            $table->integer('mp3_filesize')->nullable();
            $table->string('video_url')->nullable();
            $table->string('video_duration')->nullable();
            $table->integer('video_filesize')->nullable();
            $table->json('other_urls')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('media_items');
    }
};
